<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List</title>
    <ul style="list-style-type:circle" ">
        <li>Bangladesh</li>
        <li>England</li>
        <li>Afganisthan</li>
    </ul>
    <ul style="list-style-type:square">
    <li>Bangladesh</li>
    <li>England</li>
    <li>Afganisthan</li>
    </ul>
    <ul>
        <li>Bangladesh</li>
        <li>England</li>
        <li>Afganisthan</li>
    </ul>
    <ul style="list-style-type:disc">
        <li>Bangladesh</li>
        <li>England</li>
        <li>Afganisthan</li>
    </ul>
</head>
<body>

</body>
</html>