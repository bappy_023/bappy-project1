<?php

class Calculator
{
    private $num1 ="";
    private $num2 ="";
    public function setData($data)
    {
        $this->num1 =$data['num1'];
        $this->num2 =$data['num2'];
    }
    public function getData()
    {
        echo "<pre>";
        $this->add();
        $this->sub();
        $this->mul();
        $this->div();
    }
    public function add()
    {
        echo "Result of addition : ".($this->num1 + $this->num2)."<br>";
    }
    public function sub()
    {
        echo "Result of Subtraction : ".($this->num1 - $this->num2)."<br>";
    }
    public function mul()
    {
        echo "Result of Multiplication : ".($this->num1 * $this->num2)."<br>";
    }
    public function div()
    {
        echo "Result of Division : ".($this->num1 / $this->num2);
    }
}