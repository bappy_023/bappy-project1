<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Html form tag</title>
    <style>
        body{margin: 180px 550px}
        h1{padding-left: 80px;}
        table tr td{padding:18px;}
        .container{width: 450px; height: 100px; background-color: #218bcc; padding: 8px}
        input[type = submit]{margin-left: 300px}
    </style>
</head>
<body>
<form  class="form-horizontal">
    <div class="container">
        <h1>Hangki-Pangki.com</h1>
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required="required">
    </div>
    <div class="container">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        <div>
        <label for="exampleInputFile">File input</label>
        <input type="file" id="exampleInputFile">
        </div>
        <p class="help-block"><mark>Example block-level help text here.</p></mark>
    </div>
    <div class="container">
        <label>
            <input type="checkbox"> Check me out
        </label>
        <div>
        <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </div>

</form>

</body>
</html>