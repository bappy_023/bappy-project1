<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT']."/bappy-project1/AtomicProjectB31/Vendor/autoload.php");

use App\SEIP127665\Mobile\Mobile;
use App\SEIP127665\Message\Message;
$obj =new Mobile();
$allResult=$obj->index();
//\App\SEIP127665\Utility\Utility::dd($allResult);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Application</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div id="success_message">
        <?php
        if (array_key_exists('success_message',$_SESSION) && !empty($_SESSION['success_message']))
        {
            echo Message::message();
        }
        ?>
    </div>
    <div class="form-group">
    <h2>All Mobile List</h2>
    </div>
        <div class="form-group">
            <a href="create.php" class="btn btn-info" role="button">Add again</a>
        </div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Mobile Name</th>
            <th>Model No</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $sl=0; foreach($allResult as $result){$sl++; ?>
        <tr>
            <td><?php echo $sl ?></td>
            <td><?php echo $result->id ?></td>
            <td><?php echo $result->mobile_name ?></td>
            <td><?php echo $result->model_no ?></td>
            <td><a href="view.php" class="btn btn-info" role="button">View</a>
                <a href="edit.php" class="btn btn-success" role="button">Edit</a>
                <a href="delete.php" class="btn btn-danger" role="button">Delete</a>
            </td>
        </tr>

        <?php } ?>

        </tbody>
    </table>
</div>
</body>
</html>
