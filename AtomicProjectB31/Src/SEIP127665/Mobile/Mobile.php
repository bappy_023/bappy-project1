<?php

namespace App\SEIP127665\Mobile;
use App\SEIP127665\Utility\Utility;
use PDO;
use PDOException;
use App\SEIP127665\Message\Message;
class Mobile
{
    public $id ="";
    public $mobile_name ="";
    public $model_no="";
    public $serverName="localhost";
    public $databaseName="atomic project";
    public $user="root";
    public $pass="";
    public $conn;


    public function __construct()
    {
        try {
# MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=$this->serverName;dbname=$this->databaseName", $this->user, $this->pass);
            }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }

   public function setData($data="")
    {
        if(array_key_exists('mobile_name',$data) && !empty($data))
        {
            $this->mobile_name=$data['mobile_name'];
        }
        if(array_key_exists('model_no',$data) && !empty($data))
        {
            $this->model_no=$data['model_no'];
        }
        return $this;
    }
    public function store()
    {
        $query="INSERT INTO `mobile` (`mobile_name`, `model_no`) VALUES (:mobile_name,:model_no);";
        $statement=$this->conn->prepare($query);
        $result=$statement->execute(array(":mobile_name"=>$this->mobile_name,":model_no"=>$this->model_no));
        if ($result) {

            header('Location:index.php');
            Message::message('Data has been inserted successfully');
            Utility::reDirect('index.php');
        }
        else
        {
            Message::message('Data has been not inserted succesfully');
            Utility::reDirect('index.php');
        }
    }
    public function index()
    {
        $query="SELECT * FROM `mobile` ";
        $statement=$this->conn->query($query);
        $_allData=$statement->fetchAll(PDO::FETCH_OBJ);
        return $_allData;
    }
}










