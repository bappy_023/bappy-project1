<?php


abstract class classManager
{
    public abstract function setPower();
    public abstract function getPower();
}
abstract class infoManager
{
    public abstract function setInfo();
    public abstract function getInfo();
}

class subManager extends classManager
{
    public function setPower()
    {
        echo "This is for setPower from abstract class<br>";
        // TODO: Implement setPower() method.
    }
    public function getPower()
    {
        echo "This is for getPower from abstract class<br>";
        // TODO: Implement getPower() method.
    }
}

class subInfo extends infoManager
{
    public function setInfo()
    {
        echo "This is for setinfo from abstract class<br>";
        // TODO: Implement setInfo() method.
    }
    public function getInfo()
    {
        echo "This is for getinfo from abstract class";
        // TODO: Implement getInfo() method.
    }
}

subManager::setPower();
subManager::getpower();
subInfo::setinfo();
subInfo::getinfo();
