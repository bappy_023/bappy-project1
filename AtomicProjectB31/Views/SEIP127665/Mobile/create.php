<!DOCTYPE html>
<html lang="en">
<head>
    <title>Application</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Mobile add from</h2>
    <form action="store.php" method="post">
        <div class="form-group">
            <label for="mobile_name">Title :</label>
            <input type="text" class="form-control" id="mobile_name" name="mobile_name" placeholder="Enter Mobile Name" required="required" value="" >
        </div>
        <div class="form-group">
        <label for="model_no">Model No :</label>
        <input type="text" class="form-control" id="model_no" name="model_no" placeholder="Enter Model No"  required="required" value="">
        </div>
        <button type="submit" class="btn btn-info">Submit</button>
    </form>
</div>

</body>
</html>
