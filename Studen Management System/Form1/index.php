<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login-Form</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/form.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body{margin: 200px 550px}
        h2{padding-left: 200px}
        table tr td{padding: 7px}
        .container{width: 500px; height: 350px; background-color: rgba(176, 212, 191, 0.95); padding: 20px}
        input[type = submit]{margin-left: 125px}
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 col-md-offset-2">
            <h1>Login</h1>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="background">
        <div class="form-group">
            <div class="row">
                <div class="col-md-3">
                    <label for="Email">Email </label>
                </div>
                <div class="form-inline">
                    <div class="col-md-3">
                    <input type="text" name="Email" id="Email"  class="form-control" placeholder="Enter Your Email" required="required">
                    </div>
                </div>
            </div>
             <div class="row">
                 <div class="col-md-3">
                     <label for="Password">Password </label>
                 </div>
                 <div class="form-inline">
                     <div class="col-md-3">
                         <input type="password" name="Password" id="Password"  class="form-control" placeholder="Enter Your Password" required="required">
                     </div>
                 </div>
             </div>
                <div class="col-md-8">
                    <div class="col-md-5"></div>
                    <label>
                        <input type="checkbox"> Remember Me
                    </label>
                </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 col-md-8"></div>
                    <button type="submit" class="btn btn-primary">Log In</button>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <p class="text-center"><a href="#"   data-toggle="modal" data-target="#login-modal">Forgot Password?</a></p>
                    </div>
                    <div class="col-md-6">
                        <button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>











