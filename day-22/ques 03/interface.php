<?php

interface a
{
    public function a1();

    public function a2();
}

interface b
{
    public function b1();
    public function b2();
}

interface c
{
    public function c1();
    public function c2();
}

interface d
{
    public function d1();
    public function d2();
}
class subInterface implements a,b,c,d
{
    public function a1()

    {
        echo "* I'm from a1 of interface a<br>";
    }
    public function a2()
    {
        echo "* I'm from a2 of interface a<br>";
    }
    public function b1()
    {
        echo "* I'm from b1 of interface b<br>";
    }
    public function b2()
    {
       echo "* I'm from b2 of interface b<br>";
    }
    public function c1()
    {
        echo "* I'm from c1 of interface c<br>";
    }
    public function c2()
    {
        echo "* I'm from c2 of interface c<br>";
    }
    public function d1()
    {
        echo "* I'm from d1 of interface d<br>";
    }
    public function d2()
    {
        echo "* I'm from d2 of interface d";
    }
}
subInterface::a1();
subInterface::a2();
subInterface::b1();
subInterface::b2();
subInterface::c1();
subInterface::c2();
subInterface::d1();
subInterface::d2();
