<?php
$all = array (
    "fruits"  => array("1" => "orange", "3" => "banana", "2" => "apple"),
    "numbers" => array(1, 2, 3, 4, 5, 6),
    "holes"   => array("first", 4 => "second", "third")
);
echo "<pre/>";
print_r($all);
echo "<br/>";

$array = array(1, 1, 1, 1,  1, 8 => 1,  5 => 1,19, 3=>13);
echo "<pre>";
print_r($array );
echo "<br/>";
$firstquarter = array( 'January',1 => 'February', 'March');
print_r($firstquarter);
echo "<br/>";
$foo = array('bar' => 'baz');
echo "Hello {$foo['bar']}!"; // Hello baz!
?>