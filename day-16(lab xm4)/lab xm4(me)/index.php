<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Appendix A-Sample US Death Certificate Form</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/form.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 col-md-offset-1">
            <h1>Appendix A-Sample US Death Certificate Form</h1>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>The sample death reporting form included in the content profile reflects much of the data captured for the U.S.
                Standard Certificate of Death.However, the VRDR content profile may be modified to include and accommodate international death reporting requirements.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-3 col-md-offset-1"></div>
        <div class="col-md-offset-5">
            <h2>Death Reporting For Vital Record</h2>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>Decendent's Name(Include AKA's if any)</h3>
        </div>
    </div>
    <div class="background">
        <div class="form-group">
            <div class="row">
                <div class="col-md-3">
                    <label for="last_name">Last Name </label>
                    <input type="text" name="last_name" id="last_name" autofocus="autofocus" class="form-control">
                </div>
                <div class="col-md-6">
                    <label for="first_name">First Name </label>
                    <input type="text" name="first_name" id="first_name" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="middle_name">Middle Name </label>
                    <input type="text" name="middle_name" id="middle_name" class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-3">
                    <label for="date_birth">Date of Birth</label>
                    <input type="date" name="birth_date" id="birth_date" class="form-control">
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                    <label for="gender">Gender</label>
                    <select name="" id="" class="form-control">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                    </div>
                    <div class="col-md-6">
                        <label for="social_number">Social Security Number </label>
                        <input type="number" name="social_number" id="social_number" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="facility_name">Facility Name </label>
                    <input type="text" name="Facility Name" id="facility_name" class="form-control">
                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12">
                <div class="line">
                <h4>Decedent of Hispanic Origin</h4>
                    <hr style="text-align:right">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <p>Check the box that best describes whether the decendent is Spanish / Hispanic / Latino. Check the "No" box if decendent is not Spanish / Hispanic / Latino:</p>
        </div>
        <div class="row">
            <div class="radio">
                <label for="no">
                <input type="radio" name="radio" id="radio" value="no">
                No, not Spanish/Hispano/Latino
                </label>
                <label for="yes" class="col-md-offset-1">
                    <input type="radio" name="radio" id="yes" value="yes">
                    Yes, Mexican, Mexican American, Chicano
                </label>
                <label for="yes" class="col-md-offset-1">
                    <input type="radio" name="radio" id="yes" value="yes">
                    Yes, Puerto Rican
                </label>
                <label for="yes" class="col-md-offset-1">
                    <input type="radio" name="radio" id="yes" value="yes">
                    Yes, Cuban
                </label>
            </div>
        </div>
        <div class="row">
            <div class="radio">
                <label for="other">
                    <input type="radio" name="radio" id="other" value="other">
                    Yes, other Spanish/Hispano/Latino(Specify):
                </label>
            </div>
        </div>
        <div class="row col-md-offset-1">
            <input type="text" name="other" class="form-control" value="">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="line">
                    <hr>
                    <h4>Decedent's Rate</h4>
                    <hr style="text-align:right">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <p>Check one or more races to indicate what the Decedent considered himself or herself to be:</p>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label>
                    <input type="radio" name="" id="white" value="">
                    White
                </label>
            </div>
            <div class="col-md-4">
                <label>
                    <input type="radio" name="" id="filipino" value="">
                    Filipino
                </label>
            </div>
            <div class="col-md-4">
                <label>
                    <input type="radio" name="" id="native" value="">
                    Native Hawaiian
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="black">
                    <input type="radio" name="" id="black" value="">
                    Black or African
                </label>
            </div>
            <div class="col-md-4">
                <label for="japanese">
                    <input type="radio" name="" id="japanese" value="">
                    Japanese
                </label>
            </div>
            <div class="col-md-4">
                <label for="guamanian">
                    <input type="radio" name="" id="guamanian" value="">
                    Guamanian or Chamorro
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="american">
                    <input type="radio" name="" id="american">
                    American Indian or Alaska Native
                </label>
            </div>
            <div class="col-md-4">
                <label for="korean">
                    <input type="radio" name="" id="korean">
                    Korean
                </label>
            </div>
            <div class="col-md-4">
                <label for="samoan">
                    <input type="radio" name="" id="samoan">
                    Samoan
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="tribe" class="radio-inline">
                    (Name of the enrolled or principal tribe)
                </label>
            </div>
            <div class="col-md-4">
                <label>
                    <input type="radio" name="" id="viatnamese">
                    Viatnamese
                </label>
            </div>
          <div class="col-md-4">
                <label>
                    <input type="radio" name="" id="islander">
                    Other Pacific Islander(Specify)
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label>
                    <input type="text" id="tribe" class="form-control">
                </label>
            </div>
            <div class="col-md-4">
                <label>
                    <input type="radio" name="" id="other">
                    Other Asian(Specify)
                </label>
            </div>
            <div class="col-lg-3">
                <label for="other">
                    <input type="radio" name="" id="other" class="form-inline">
                    Other (Specify)
                </label>
            </div>
            <div class="col-md-4">
                <label for="tribe" class="radio-inline">
                    <input type="text" id="tribe" class="form-control">
                </label>
            </div>
            <div class="col-md-4">
                <label for="tribe" class="radio-inline">
                    <input type="text" id="tribe" class="form-control">
                </label>
            </div>
        </div>
    <div class="row">
        <div class="col-md-4">
            <label for="asian">
                <input type="radio" name="" id="asian">
                Asian Indian
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <label for="chines">
                <input type="radio" name="" id="chines">
                Chines
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="line">
                <hr>
                <h4>Item Must be Completed by Person Who Pronounces or Certifies Death.</h4>
                <hr style="text-align:right">
            </div>
        </div>
    </div>
    <div class="background">
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <label for="date">Date Pronounced Dead</label>
                    <input type="text" name="" id="date" autofocus="autofocus" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="time">Time Pronounced Dead </label>
                    <input type="text" name="" id="time" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="signature">Signature or Person pronouncing Death </label>
                    <input type="text" name="" id="signature" class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <label for="license">License Number</label>
                    <input type="number" name="" id="license" class="form-control">
                </div>
                <div class="col-md-4">
                        <label for="date">Social Security Number </label>
                        <input type="date" name="" id="date" class="form-control">
                    </div>
                <div class="col-md-4">
                    <label for="actual">Actual Or Presumed Date Of Birth</label>
                    <input type="date" name="" id="actual" class="form-control">
                </div>
                </div>
                <div class="col-md-4">
                    <label for="actual">Actual Or Presumed Date Of Birth</label>
                    <input type="date" name="" id="actual" class="form-control">
                </div>
            <div class="row">
                <div class="radio">
                    <label class="text"><h5>Was Madical Examiner Or Corner Contacted?</h5>
                    </label>
                    <div class="radio">
                    <label for="yes" class="under-line">
                        <input type="radio" name="radio" id="yes" value="yes">
                        Yes
                    </label>
                        <label for="no" class="under-line">
                            <input type="radio" name="radio" id="no" value="yes">
                            No
                        </label>
                    </div>
                </div>
            </div>
        </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="line">
                <hr>
                <h4>Cause Of Death(See instruction and example)</h4>
                <hr style="text-align:right">
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <p><h3>PART 1.</h3> Enter the chain of event--diseases, injuries, or complitions--that directly caused
        the death.DO NOT enter terminal events such as Approximateinterval:cardiac arrest respiratory arrest. or
        ventricular fibrillation without showing the etiology.DO NOT ABBREVIATE Enter only cause on a line.
        Add additional lines if necessary
    </div>
    <div></div>
    <div class="background">
        <div class="form-group">
            <div class="row">
                <div class="col-md-5">
                    <label>a.Immediate Cause(Final dirase or condition resulting in death)</label>
                    <input type="text" name="" id="date" autofocus="autofocus" class="form-control">
                </div>
                <div class="col-md-4">
                    <label>Due to(or as a consequence of):</label>
                    <input type="text" name="" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>Onset to death</label>
                    <input type="text" name=""  class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-5">
                    <label>b.Sequentially List Conditions,(if any,leading to the cause listed on line a.)</label>
                    <input type="text" name="" class="form-control">
                </div>
                <div class="col-md-4">
                    <label>Due to(or as a consequence of):</label>
                    <input type="text" name="" id="date" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>Onset to death</label>
                    <input type="text" name="" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <label>c.Enter the Underiying Cause,(disease or injury that ibitiated the events resulting in death)</label>
                    <input type="text" name=""  class="form-control">
                </div>
                <div class="col-md-4">
                    <label>Due to(or as a consequence of):</label>
                    <input type="text" name="" id="date" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>Onset to death</label>
                    <input type="text" name="" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <label>c.Last</label>
                    <input type="text" name=""  class="form-control">
                </div>
                <div class="col-md-3">
                    <label>Onset to death</label>
                    <input type="text" name="" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <p><h3>PART 2.</h3>Enter other significant condition contributing to death but not resulting
            in the underiying cause given in PART 1</p>
        </div>
        <div class="row">
            <div class="col-md-6">
                 <textarea rows="4" cols="130" class="active"></textarea>
            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>











