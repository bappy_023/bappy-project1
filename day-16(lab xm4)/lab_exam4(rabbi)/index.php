<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/form.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 col-md-offset-1">
                <h2>Appendix A-Sample US Death Certificate Form </h2>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <p>The sample death reporting form included in the content profile reflects much of the data captured for the U.S. Standard Certificate of Death. However, the VRDR content profile may be modified to include and accommodate international death reporting requirements.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-md-offset-1"></div>
            <div class="col-md-5">
                <h3>Death Reporting For Vital Record</h3>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4>Decendent's Name(Include AKA's if any)</h4>
            </div>
        </div>
        <div class="background">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <label for="last_name">Last Name </label>
                        <input type="text" name="last_name" id="last_name" autofocus="autofocus" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="first_name">First Name </label>
                        <input type="text" name="first_name" id="first_name" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label for="middle_name">Middle Name </label>
                        <input type="text" name="middle_name" id="middle_name" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <label for="birth_date">Death of Birth </label>
                        <input type="date" name="birth_date" id="birth_date" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-6">
                            <label for="gender">Gender </label>
                            <select name="" id="" class="form-control">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="social_number">Social Security Number </label>
                            <input type="number" name="social_number" id="social_number" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="facility_name">Facility Name </label>
                        <input type="text" name="facility_name" id="facility_name" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4>Decedent of Hispanic Origin</h4>
            </div>
        </div>
        <div class="row">
            <hr>
        </div>
        <div class="row">
            <p class="check">Check the box that best describes whether the decendent is Spanish / Hispanic / Latino. Check the "No" box if decendent is not Spanish / Hispanic / Latino:</p>
        </div>

        <div class="row">
            <div class="radio">
                <label for="no">
                    <input type="radio" name="radio" id="no" value="no">
                    No, not Spanish/Hispano/Latino
                </label>
                <label for="yes" class="col-md-offset-1">
                    <input type="radio" name="radio" id="yes" value="yes">
                    Yes, Mexican, Mexican American, Chicano
                </label>
                <label for="puerto" class="col-md-offset-1">
                    <input type="radio" name="radio" id="poerto" value="puerto">
                    Yes, Puerto Rican
                </label>
                <label for="cuban" class="col-md-offset-1">
                    <input type="radio" name="radio" id="cuban" value="cuban">
                    Yes, Cuban
                </label>
            </div>
        </div>
        <div class="row">
            <div class="radio">
                <label for="other">
                    <input type="radio" name="radio" id="other" value="other">
                    Yes, other Spanish/Hispano/Latino(Specify):
                </label>
            </div>
        </div>
        <div class="row col-md-offset-1">
            <input type="text" name="other" class="form-control" value="">
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4 class="gap">Decedent's Race</h4>
            </div>
        </div>
        <div class="row">
            <hr>
        </div>
        <div class="row">
            <p class="check">Check one or more races to indicate what the Decedent considered himself or herself to be:</p>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="white" class="radio-inline">
                    <input type="radio" name="" id="white" value="white">
                    White
                </label>


            </div>
            <div class="col-md-4">
                <label for="filipino" class="radio-inline">
                    <input type="radio" name="" id="filipino" value="filipino">
                    Filipino
                </label></div>
            <div class="col-md-4">
                <label for="native" class="radio-inline">
                    <input type="radio" name="" id="native" value="native">
                    Native Hawaiian
                </label>
            </div>


        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="black" class="radio-inline">
                    <input type="radio" name="" id="black" value="black">
                    Black African American
                </label>
            </div>
            <div class="col-md-4">
                <label for="japanese" class="radio-inline">
                    <input type="radio" name="" id="japanese" value="japanese">
                    Japanese
                </label>
            </div>
            <div class="col-md-4">
                <label for="chamorro" class="radio-inline">
                    <input type="radio" name="" id="chamorro" value="chamorro">
                    Guamanian or Chamorro
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="Indian" class="radio-inline">
                    <input type="radio" name="" id="indian" value="indian">
                    American Indian or Alaska Native
                </label>
            </div>
            <div class="col-md-4">
                <label for="korean" class="radio-inline">
                    <input type="radio" name="" id="korean" value="korean">
                    Japanese
                </label>
            </div>
            <div class="col-md-4">
                <label for="samoan" class="radio-inline">
                    <input type="radio" name="" id="samoan" value="samoan">
                    Samoan
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="tribe" class="radio-inline">
                    (Name of the enrolled or principal tribe)
                </label>
            </div>
            <div class="col-md-4">
                <label for="viatnamese" class="radio-inline">
                    <input type="radio" name="" id="viatnamese" value="viatnamese">
                    Viatnamese
                </label>
            </div>
            <div class="col-md-4">
                <label for="islander" class="radio-inline">
                    <input type="radio" name="" id="islander" value="islander">
                    Other Pacific Islander(Specify)
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="tribe" class="radio-inline">
                    <input type="text" id="tribe" class="form-control">
                </label>
            </div>
            <div class="col-md-4">
                <label for="asian" class="radio-inline">
                    <input type="radio" name="" id="asian" value="asian">
                    Other Asian(Specify)
                </label>
            </div>
            <div class="col-md-4">
                <label for="islander" class="radio-inline">
                    <input type="text" id="" class="form-control">
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4 class="gap">Items Must be Completed by Person Who Pronounces or Certifies Death</h4>
            </div>
        </div>
        <div class="row">
            <hr>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-3">
                    <label for="pronounced_date">Date Pronounced Dead</label>
                    <input type="date" name="last_name" id="pronounced_date" autofocus="autofocus" class="form-control">
                </div>
                <div class="col-md-6">
                    <label for="first_name">Time Pronounced Dead </label>
                    <input type="text" name="first_name" id="first_name" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="middle_name">Signature or Person pronouncing Death </label>
                    <input type="text" name="middle_name" id="middle_name" class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-3">
                    <label for="birth_date">License Number </label>
                    <input type="number" name="birth_date" id="birth_date" class="form-control">
                </div>
                <div class="col-md-6">
                    <label for="birth_date">Date Signed </label>
                    <input type="date" name="birth_date" id="birth_date" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="facility_name">Actual or Presumed Date of Birth </label>
                    <input type="text" name="facility_name" id="facility_name" class="form-control">
                </div>
            </div>
        </div>
    </div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>