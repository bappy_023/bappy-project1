<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style>
        body{margin: 200px 550px}
        h2{padding-left: 200px}
        table tr td{padding: 7px}
        .container{width: 500px; height: 350px; background-color: rgba(176, 212, 191, 0.95); padding: 20px}
        input[type = submit]{margin-left: 125px}
    </style>
</head>
<body>
<form>
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 col-md-offset-2">
            <h1>Login</h1>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="form-group">
        <div class="checkbox">
        <label>
            <input type="checkbox"> Remember Me
        </label>
            <div class="form-inline">
            <p class="text-center"><a href="#"   data-toggle="modal" data-target="#login-modal">Forgot Password?</a></p>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Log In</button>
</div>
</form>

</body>
</html>