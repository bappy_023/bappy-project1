<?php

class Cricket
{
    public function __construct()
    {
        echo "I'm a constructor of Cricket class";
    }
}
class Movie extends Cricket
{
    public function __construct()
    {
        echo "I'm a constructor of Movie Class <br>";
        parent::__construct();
    }
}
$obj =new Movie();
?>
